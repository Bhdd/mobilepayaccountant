﻿using System;
using SimpleInjector;

namespace MobilePayAccountant
{
  public class Bootstrap
  {
    public static Container container;

    public static void Start()
    {
      container = new Container();
      var lifestyle = Lifestyle.Singleton;
      container.Register<ITransactionDataAccess, TransactionFileDataAccess>(lifestyle);
      container.Verify();
    }
  }

  internal class Program
  {
    private static void Main(string[] args)
    {
      Bootstrap.Start();
      var BL = Bootstrap.container.GetInstance<TransactionHandler>();
      BL.GenerateOutput();
      Console.ReadLine();
    }
  }
}