﻿using System.Collections.Generic;
using MobilePayAccountant.Model;

namespace MobilePayAccountant.Utils
{
  public static class Tools
  {
    public static bool FixedInvoiceAlreadyCalculated(Transaction transaction,
      List<Transaction> currentMonthTransactionList)
    {
      return currentMonthTransactionList.Exists(x => x.MerchantName == transaction.MerchantName) ? true : false;
    }
  }
}