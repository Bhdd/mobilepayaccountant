﻿using System.Collections.Generic;

namespace MobilePayAccountant.Utils
{
  public static class Initialization
  {
    public const decimal InvoiceFixedFee = 29;
    public const decimal TransactionPercentageFee = (decimal) 0.01;

    public static readonly Dictionary<string, decimal> MerchantDiscount = new Dictionary<string, decimal>
    {
      {"TELIA", (decimal) 0.10}, {"CIRCLE_K", (decimal) 0.20}
    };
  }
}