﻿using System.IO;
using MobilePayAccountant.Model;

namespace MobilePayAccountant
{
  public interface ITransactionDataAccess
  {
    StreamReader TransactionFile { get; }

    Transaction PopulateTransactionFromTextLine();
  }
}