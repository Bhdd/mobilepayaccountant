﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using MobilePayAccountant.Model;

namespace MobilePayAccountant
{
  public class TransactionFileDataAccess : ITransactionDataAccess
  {
    public StreamReader TransactionFile { get; } = new StreamReader(@"Data\transactions.txt");

    public Transaction PopulateTransactionFromTextLine()
    {
      var sr = TransactionFile;
      var transaction = new Transaction();
      var line = string.Empty;
      if (!sr.EndOfStream && !string.IsNullOrEmpty(line = sr.ReadLine().Trim()))
      {
        var lineData = Regex.Split(line, @"\s+");
        var transactionDate = DateTime.MinValue;
        DateTime.TryParse(lineData[0], out transactionDate);
        decimal amount = -1;
        decimal.TryParse(lineData[2], out amount);
        var Merchant = lineData[1].Trim();

        if (transactionDate > DateTime.MinValue && amount >= 0)
          transaction = new Transaction {TransactionDate = transactionDate, MerchantName = Merchant, Amount = amount};
      }

      return transaction;
    }
  }
}