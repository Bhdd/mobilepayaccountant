﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MobilePayAccountant.Model;
using MobilePayAccountant.Utils;

namespace MobilePayAccountant
{
  public class TransactionHandler
  {
    private readonly ITransactionDataAccess _objtransaction;

    public TransactionHandler(ITransactionDataAccess objtransaction)
    {
      _objtransaction = objtransaction;
    }

    public StreamReader GetFile()
    {
      return _objtransaction.TransactionFile;
    }

    private Transaction PopulateTransactionFromTextLine()
    {
      return _objtransaction.PopulateTransactionFromTextLine();
    }

    public Transaction CalculateFeeWithFixedInvoice(List<Transaction> currentMonthTransactionList)
    {
      decimal fee = -1;
      var transaction = PopulateTransactionFromTextLine();
      if (!string.IsNullOrEmpty(transaction.MerchantName))
      {
        if (Tools.FixedInvoiceAlreadyCalculated(transaction, currentMonthTransactionList))
          fee = CalculateFee(transaction, true);
        else
          fee = CalculateFee(transaction, false);
        transaction.Amount = fee;
      }

      return transaction;
    }

    public decimal CalculateFee(Transaction transaction, bool firstTransaction = false)
    {
      var fee = transaction.Amount * Initialization.TransactionPercentageFee;
      if (Initialization.MerchantDiscount.Keys.Contains(transaction.MerchantName))
      {
        var discountRate = Initialization.MerchantDiscount.FirstOrDefault(x => x.Key == transaction.MerchantName).Value;

        fee = fee - fee * discountRate;
      }

      if (!firstTransaction)
        if (fee > 0)
          fee += Initialization.InvoiceFixedFee;
      return fee;
    }

    public void GenerateOutput()
    {
      var file = GetFile();
      while (!file.EndOfStream)
      {
        var line = string.Empty;
        var currentMonthTransactionList = new List<Transaction>();
        var transaction = CalculateFeeWithFixedInvoice(currentMonthTransactionList);

        while (!string.IsNullOrEmpty(transaction.MerchantName))
        {
          var outLine = string.Empty;
          outLine += transaction.TransactionDate.ToString("MM/dd/yyyy") + " " + transaction.MerchantName + " " +
                     Math.Round(transaction.Amount, 2);
          currentMonthTransactionList.Add(transaction);
          transaction = CalculateFeeWithFixedInvoice(currentMonthTransactionList);
          Console.WriteLine(outLine);
        }

        currentMonthTransactionList.Clear();
        Console.WriteLine();
      }

      file.Close();
    }
  }
}