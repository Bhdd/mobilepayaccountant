﻿using System;

namespace MobilePayAccountant.Model
{
  public class Transaction
  {
    public DateTime TransactionDate { get; set; }
    public string MerchantName { get; set; }
    public decimal Amount { get; set; }
  }
}