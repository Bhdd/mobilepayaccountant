﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MobilePayAccountant;
using MobilePayAccountant.Model;
using MobilePayAccountant.Utils;
using Moq;

namespace MobilePayAccountantTest
{
  [TestClass]
  public class UnitTest
  {
    private Mock<ITransactionDataAccess> _mock;
    private TransactionHandler _target;

    [TestMethod]
    public void FixedInvoiceAlreadyCalculated_Test()
    {
      var transList = new List<Transaction>
      {
        new Transaction {TransactionDate = DateTime.Parse("02/05/2018"), MerchantName = "CIRCLE_K", Amount = 100},
        new Transaction {TransactionDate = DateTime.Parse("02/10/2018"), MerchantName = "7-ELEVEN", Amount = 100}
      };
      var t = new Transaction {TransactionDate = DateTime.Parse("02/05/2018"), MerchantName = "7-ELEVEN", Amount = 90};
      Assert.IsTrue(Tools.FixedInvoiceAlreadyCalculated(t, transList));
    }

    [TestMethod]
    public void CalculateFee_Test()
    {
      var tr = new Transaction
        {TransactionDate = DateTime.Parse("10/25/2018"), MerchantName = "CIRCLE_K", Amount = 100};
      var fee = Initialization.TransactionPercentageFee * tr.Amount;
      fee = Math.Round(fee - (decimal) 0.2 * fee, 2);
      Bootstrap.Start();
      var BL = Bootstrap.container.GetInstance<TransactionHandler>();
      Assert.AreEqual(fee, BL.CalculateFee(tr, true));
    }

    [TestMethod]
    public void CalculateFeeWithFixedInvoice_Test()
    {
      var tr = new Transaction
        {TransactionDate = DateTime.Parse("10/25/2018"), MerchantName = "CIRCLE_K", Amount = 100};
      var transactionList = new List<Transaction>
        {tr, new Transaction {TransactionDate = DateTime.Parse("10/26/2018"), MerchantName = "TELIA", Amount = 100}};
      _mock = new Mock<ITransactionDataAccess> {CallBase = true};
      _target = new TransactionHandler(_mock.Object);

      _mock.SetupGet(x => x.TransactionFile).Returns(It.IsAny<StreamReader>());
      _mock.Setup(x => x.PopulateTransactionFromTextLine()).Returns(tr);
      var fee = Initialization.TransactionPercentageFee * tr.Amount;
      fee = Math.Round(fee - (decimal) 0.2 * fee, 2);
      var t = _target.CalculateFeeWithFixedInvoice(transactionList);
      Assert.AreEqual(fee, t.Amount);
    }
  }
}